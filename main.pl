#!/usr/bin/perl
use strict;
use warnings; 

use WWW::Mechanize;
use HTML::Grabber;
use Data::Dumper;
use JSON;
use URI::Escape;
my $video_url = $ARGV[0];
print $video_url;
my $crawler = WWW::Mechanize -> new();
$crawler -> get($video_url);
my $youtube_dom = HTML::Grabber -> new(html => $crawler -> content());
my $video_data_source_json = $youtube_dom -> find('#player-mole-container > script:nth-child(4)') -> text();
$video_data_source_json =~ s/^.*?ytplayer\.config = //g;
$video_data_source_json =~ s/;]]><\/script>$//;
$video_data_source_json =~ s/;ytplayer\.load.*$//;
#~ print $video_data_source_json;
my $video_data = decode_json $video_data_source_json;
my @video_streams = split /,/, $video_data -> {'args'} -> {'url_encoded_fmt_stream_map'};
my @streams;
foreach (@video_streams) {
	my $stream_info = $_;
	for( split /&/, $stream_info ) {
		my ($key, $value) = split /=/, $_;
		print "$value " if $key eq 'quality';
		print uri_unescape($value), "\n" if $key eq "url";
	}
	#~ push @streams, %stream;
}

#~ foreach (@streams) {
	#~ print $_ -> {'quality'},' - ', $_ -> {'url'};
#~ }~
#~ print Dumper @video_streams;


